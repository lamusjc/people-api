package com.test.people.model;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name = "personas")
public class People {
 
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
 
  @Column(name = "nombre")
  private String nombre;
 
  @Column(name = "apellido")
  private String apellido;
 
  @Column(name = "cedula")
  private String cedula;
 
  public People() {
  }
 
  public People(String nombre, String apellido, String cedula) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.cedula = cedula;
  }
 
  public long getId() {
    return id;
  }
 
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
 
  public String getNombre() {
    return this.nombre;
  }
 
  public void setApellido(String apellido) {
    this.apellido = apellido;
  }
 
  public String getApellido() {
    return this.apellido;
  }
 
  public void setCedula(String cedula) {
	  this.cedula = cedula;
  }
  
  public String getCedula() {
    return this.cedula;
  }
 
  @Override
  public String toString() {
    return "People [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", cedula=" + cedula + "]";
  }
}