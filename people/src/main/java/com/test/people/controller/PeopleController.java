package com.test.people.controller;
 
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
import com.test.people.model.People;
import com.test.people.repo.PeopleRepository;;
 
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class PeopleController {
  @Autowired
  PeopleRepository repository;
 
  @CrossOrigin(origins = "*")
  @GetMapping("/people")
  public List<People> getAllCustomers() {
    System.out.println("Obtener todas las personas");
 
    List<People> people = new ArrayList<>();
    repository.findAll().forEach(people::add);
 
    return people;
  }
 
  @PostMapping(value = "/people/create")
  public People postPeople(@RequestBody People people) {
    People _people = repository.save(
    		new People(	people.getNombre(), 
    					people.getApellido(), 
    					people.getCedula()
    				)
    		);
    return _people;
  }
 
  @DeleteMapping("/people/{id}")
  public ResponseEntity<String> deletePeople(@PathVariable("id") long id) {
    System.out.println("Eliminado persona con ID = " + id + "...");
 
    repository.deleteById(id);
 
    return new ResponseEntity<>("Persona ha sido eliminado!", HttpStatus.OK);
  }
 
  @DeleteMapping("/people/delete")
  public ResponseEntity<String> deleteAllPeople() {
    System.out.println("Borrar todas las personas...");
 
    repository.deleteAll();
 
    return new ResponseEntity<>("Todas las personas han sido eliminadas!", HttpStatus.OK);
  }
 
  @GetMapping(value = "people/apellido/{apellido}")
  public List<People> findByApellido(@PathVariable String apellido) {
 
    List<People> people = repository.findByApellido(apellido);
    return people;
  }
 
  @PutMapping("/people/{id}")
  public ResponseEntity<People> updatePeople(@PathVariable("id") long id, @RequestBody People people) {
    System.out.println("Modificar personas con ID = " + id + "...");
 
    Optional<People> peopleData = repository.findById(id);
 
    if (peopleData.isPresent()) {
      People _people = peopleData.get();
      _people.setNombre(people.getNombre());
      _people.setApellido(people.getApellido());
      _people.setCedula(people.getCedula());
      return new ResponseEntity<>(repository.save(_people), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
}