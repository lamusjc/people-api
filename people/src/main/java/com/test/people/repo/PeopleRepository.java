package com.test.people.repo;
 
import java.util.List;
 
import org.springframework.data.repository.CrudRepository;
 
import com.test.people.model.People;
 
public interface PeopleRepository extends CrudRepository<People, Long> {
  List<People> findByApellido(String apellido);
}