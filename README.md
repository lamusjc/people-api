# People Api

## Modificar parametros de conexion a base de datos

En el archivo `application.properties` modificar parametros necesarios para la conexion con MySQL. Necesita MySQL, como alternativa puede usar XAMPP Control Panel v3.2.2. Inicia el servidor de Apache y MySQL.

Luego, se crea la base de datos con nombre `personas`. En el repositorio esta la carpeta llamada `database` con un archivo llamado `script.sql` solo copian, pegan e inician la consulta SQL donde le creara la tabla `personas`.

Verificar los parametros de autenticar con base de datos SQL, con username, password y por supuesto el puerto que por defecto es el 3306, en caso contrario, especificarlo en las propiedades.

## Compilar con Maven
Una vez abierto el proyecto antes de que genere errores, le da clic derecho al directorio del proyecto => Maven => Update Project.

## Iniciar Servidor
Una vez realizado todo lo anterior, se le da clic al directorio del proyecto => Run As => Spring Boot App. Por defecto escuchara el `http://localhost:8080`