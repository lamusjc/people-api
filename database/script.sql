CREATE TABLE personas (
    id int NOT NULL AUTO_INCREMENT,
    nombre varchar(250) NOT NULL,
    apellido varchar(255) NOT NULL,
    cedula varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO personas(nombre, apellido, cedula) VALUES('Julio', 'Lamus', 'V23458745');
INSERT INTO personas(nombre, apellido, cedula) VALUES('Maria', 'Lamus', 'V25854215');
INSERT INTO personas(nombre, apellido, cedula) VALUES('Jose', 'Gonzalez', 'V24125475');
INSERT INTO personas(nombre, apellido, cedula) VALUES('Jose', 'Marin', 'V4512545');
INSERT INTO personas(nombre, apellido, cedula) VALUES('Alberto', 'Torres', 'V25478451');
INSERT INTO personas(nombre, apellido, cedula) VALUES('Dianixa', 'Sanchez', 'V1478525');
INSERT INTO personas(nombre, apellido, cedula) VALUES('Andres', 'Garcia', 'V5487541');
INSERT INTO personas(nombre, apellido, cedula) VALUES('Lorena', 'Olivares', 'V2541542');
INSERT INTO personas(nombre, apellido, cedula) VALUES('Manuel', 'Ortega', 'V2356894');
INSERT INTO personas(nombre, apellido, cedula) VALUES('Simon', 'Moreno', 'V21478569');